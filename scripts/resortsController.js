angular.module('resortControllers',[])
.controller('resortCtrl',function ($scope) {

//--------------------------DatePicker Controller---------------------------//

$scope.today = function() {
	$scope.dt = new Date();
};
$scope.today();

$scope.clear = function() {
	$scope.dt = null;
};

$scope.toggleMin = function() {
	$scope.minDate = $scope.minDate ? null : new Date();
};

$scope.toggleMin();
$scope.maxDate = new Date(2020, 5, 22);

$scope.open1 = function() {
	$scope.popup1.opened = true;
};

$scope.open2 = function() {
	$scope.popup2.opened = true;
};

$scope.setDate = function(year, month, day) {
	$scope.dt = new Date(year, month, day);
};

$scope.dateOptions = {
	formatYear: 'yy',
	startingDay: 1
};

$scope.popup1 = {
	opened: false
};

$scope.popup2 = {
	opened: false
};

$scope.getDayClass = function(date, mode) {
	if (mode === 'day') {
		var dayToCheck = new Date(date).setHours(0,0,0,0);

		for (var i = 0; i < $scope.events.length; i++) {
			var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

			if (dayToCheck === currentDay) {
				return $scope.events[i].status;
			}
		}
	}

	return '';
};
});
