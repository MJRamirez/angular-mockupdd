angular.module('roomsControllers', [])
  .controller('roomCtrl', function($scope,$state,_) {

    //Scope used for filter rooms accordion   
    $scope.isCollapsed = true;

    $scope.guardarRoom = function (roomId,$scope){
        var pedidoCompleto = [];
        var rooms = JSON.parse(localStorage['rooms']);
        var roomPedido = _.where(rooms, {id: roomId});
        var pedidoTemp = [];
        pedidoTemp = JSON.parse(localStorage['pedido']);
        pedidoCompleto.push(pedidoTemp[0]); //guardo hotel solo,
        pedidoCompleto.push(pedidoTemp[1]);
        pedidoCompleto.push(roomPedido[0]);//luego le agrego habitacion para evitar duplicados o agregar +1

        localStorage['pedido'] = JSON.stringify(pedidoCompleto);
        $state.go('cart');
    }
});
