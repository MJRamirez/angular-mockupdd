var app = angular.module('oop', ['services','directivas','oitozero.ngSweetAlert','ui.router','ui.bootstrap','ngAnimate',
                                'resortControllers','panelControllers','roomsControllers','resortDetailControllers',
                                'cartControllers','ngRateIt']);
app.constant('_',
    window._
)

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('panel', {
        url: "/panel",
        templateUrl: 'views/panel.html',
    })
    .state('resorts', {
        url: "/resorts",
        templateUrl: 'views/resorts.html',
        controller : 'resortCtrl'
    })
    .state('resortdetail', {
        url: "/resortdetail/:resortId",
        templateUrl: 'views/resortdetail.html',
        controller: "resDetController"
    })
    .state('rooms', {
        url: "/resorts/:resortId/rooms",
        templateUrl: 'views/rooms.html',
        controller : 'roomCtrl'
    })
    .state('cart', {
        url: "/cart",
        templateUrl: 'views/cart.html',
        controller : 'cartCtrl'
    })
    .state('altaresort', {
        url: "/altaresort",
        templateUrl: 'views/altaResort.html',
    })
    .state('task', {
        url: "/task",
        templateUrl: 'views/task.html',
    })
    .state('editTask', {
        url: "/editTask/:0",
        templateUrl: 'views/editTask.html',
    });
    $urlRouterProvider.otherwise('/panel');
});

// Nombre de la aplicacion
app.value("appName","AngularMockupdd");
 
app.run(['$rootScope','appName',function($rootScope,appName) {
    $rootScope.appName=appName;
  
    $rootScope.__app = { 
        name: appName
    };  
  
    $rootScope.__mockupLastSaved = { 
        task: '',
        tag: ''
    };
    
    //$rootScope.__mockupLastSaved_ = __mockupLastSaved__;
}]);

// // Probando para inicialixar valores
// var resorts = [{"id":"1","name":"Hilton","addr":"Calle1","desc":"Hotel de Lujo","rate":"5","tel":"123456","map":"img/mapa1.png","img":"img/resort1.jpg"}];
// var resortsGuardar = JSON.stringify(resorts);
// localStorage.setItem("resorts",resortsGuardar);
//
// var rooms =[{"resortId":"1","id":"1","name":"Presidencial","desc":"Para el presidente","air":true,"wifi":true,"price":340,"img":"img/room1.jpg"}];
// var roomsGuardar = JSON.stringify(rooms);
// localStorage.setItem("rooms",roomsGuardar);
//
// var pedido = [{"breakfast":true},{"id":"1","name":"Hilton","addr":"Calle1","desc":"Hotel de Lujo","rate":"5","tel":"123456"},{"resortId":"1","id":"1","name":"Presidencial","desc":"Para el presidente","air":true,"wifi":true,"price":340,"img":"img/room1.jpg"}];
// var pedidoGuardar = JSON.stringify(pedido);
// localStorage.setItem("pedido",pedidoGuardar);
