var Directiva = angular.module('directivas', [])

//Dependiendo del operador que se le envie, será su comportamiento
Directiva.chooseOperBehaviour = function (operAndParam,$location) {
    var param = operAndParam.split(" ");
    var operation = param[0];
    if (operation == "pathParam") {
      var regExp = /\(([^)]+)\)/;
      var matches = regExp.exec(param[1]);
      //proceso con expresion regular el pathparam resultado queda en posicion 1
      paramPosition = matches[1];
      //separo url en partes
      var params = $location.path().split("/");
      //saco el id por posicion enviada por el pathParam
      return params[paramPosition];
    } else {
      console.log ("Funcion no implementada");
    }
}

//Actualiza el $state actual es como hacer un state.reload,  segun los valores del location sera el state que recargemos
Directiva.reload = function ($state,$location) {
    var params = {};
    var state = Directiva.chooseOperBehaviour("pathParam (1)",$location);
    var id = Directiva.chooseOperBehaviour("pathParam (2)",$location);

    if(id != undefined){
      params[0]= id;
    }else{
      params = {};
    }

    $state.go(state,params, {reload: true});
}

//Devuelve un objeto de una colección usando Id del objeto
Directiva.directive("mockupddGetDataWithId", function(storageSvc,$location) {
  return {
    scope:{
      data:'=',
      dictionary:'='
    },
    link: function(scope, element, attr) {      
      var parts = attr.mockupddGetDataWithId.split(" in ");
      var objeto = parts[0];
      var secondPart = parts[1].split(" withId ");
      var collection = secondPart[0];
      var operAndParam = secondPart[1];   

      var id = Directiva.chooseOperBehaviour(operAndParam,$location);
      elem = storageSvc.getData(collection,id);
      scope.data = elem[0];

      scope.dictionary[objeto] = elem[0];
    }
  }
})

//Devuelve todos los datos de una colección asociados a un objeto de otra colección,
//Parametros: {id, colección del objeto, colección de datos asociados}
Directiva.directive("mockupddGetDataAssociated", function(storageSvc) {
  return {
    scope:{
      data:'=',
      dictionary:'='
    },
    link: function(scope, element, attr) {
      var parts = attr.mockupddGetDataAssociated.split(" associatedWith ");
      var collection1 = parts[0];
      var collection2 = parts[1];
      scope.$watch('dictionary', function(newValue, oldValue) {
        if(typeof(scope.dictionary) != "undefined"){
          id = scope.dictionary[collection2].id;
          scope.data= storageSvc.getAssociated_(id,collection1);
        }else{
          console.log("Object no definido.");
        }
      })
    }
  }
})

//Devuelve todos los objetos de una colección
Directiva.directive("mockupddGetData", function(storageSvc) {
  return {
    scope:{
      data:'=',
    },
    link: function(scope, element, attr) {
      var collectionName = attr.mockupddGetData;
      scope.data= storageSvc.getAll(collectionName);
    }
  }
})

//Genera ID aleatorio y unico
Directiva.uniqueID = function() {
  function genRand() {
    return Math.random().toString(16).slice(-4);
  }
    return genRand() + '-' + genRand() + '-' + genRand() +
    '-' + genRand() + '-' + genRand() + genRand();
}

//Guarda objeto asociado a otro tomando el Id de la URL del navegador
Directiva.directive("mockupddSaveAssociated", function($state,storageSvc,$location) {
  return {
    link: function(scope, element, attr) {
      var parts = attr.mockupddSaveAssociated.split(" to ");
      var newObject = parts[0];
      var secondPart = parts[1].split(" associatedWith ");
      tempCollection = secondPart[0];
      operAndParam = secondPart[1];

      if (!scope[newObject]) {
        scope[newObject] = {};
      }
      $(element).on("click", function() {
        scope.$apply(function() {
        var id = Directiva.chooseOperBehaviour(operAndParam,$location);
        var collection = [];
        collection = storageSvc.getAll(tempCollection);
        scope[newObject].foreignId = id;
        collection.push(angular.copy(scope[newObject]));
        storageSvc.postTemp(tempCollection,collection);
        scope[newObject] = {};
        Directiva.reload($state,$location);
        })
      })
    }
  }
})

//Guarda objeto en una colección enviada por parametro -- JV
Directiva.directive("mockupddSave", function($state,storageSvc,$location) {
  return {
    link: function(scope, element, attr) {
      var parts = attr.mockupddSave.split(" to ");
      var newObjectName = parts[0];
      var collectionName = parts[1];
 

      if (!scope[newObjectName]) {
        scope[newObjectName] = {};
      }
      $(element).on("click", function() {
        scope.$apply(function() {
          var collection = [];
          collection = storageSvc.getAll(collectionName);
          var index = -1; 
          scope[newObjectName].id = Directiva.uniqueID();
          collection.push(angular.copy(scope[newObjectName]));
          storageSvc.postAll(collectionName,collection);

          scope.__mockupLastSaved[newObjectName] = scope[newObjectName];
          scope[newObjectName] = {};
          Directiva.reload($state,$location);
        })
      })
    }
  }
})

//Asocia objeto a otro tomando la diccionario de la aplicacion -- JV
Directiva.directive("mockupddAssociated", function($state,storageSvc,$location) {
  return {
    link: function(scope, element, attr) {
    var parts = attr.mockupddAssociated.split(" to ");
    var newObject = parts[0];   
    var secondPart = parts[1].split(".");
    var asociarAobjeto = secondPart[0];
    var collectionName = secondPart[1];   

    if (!scope[newObject]) {
      scope[newObject] = {};
    }
    $(element).on("click", function() {
    scope.$apply(function() {
      var collection = [];
      collection = storageSvc.getAll(collectionName);
      var objectToUpdate = (angular.copy(scope.__mockupLastSaved[newObject]));
      var idObjetoAasociar = scope.__mockupLastSaved[asociarAobjeto].id;
      for (var i=0; i < collection.length; i++){
        if (objectToUpdate.id==collection[i].id)
        collection[i].foreignId = idObjetoAasociar;
      }
      storageSvc.postAll(collectionName,collection);      
      Directiva.reload($state,$location);
      })
    })
    }
  }
})

//Borra objeto de una colección
Directiva.directive("mockupddDelete", function($state,storageSvc,$location) {
  return {
    link: function(scope, element, attr) {
      var parts = attr.mockupddDelete.split(" in ");
      var object = parts[0];
      var collectionName = parts[1];
      $(element).on("click", function() {
        scope.$apply(function() {
          var collection = storageSvc.getAll(collectionName);
          var objectToDelete = (angular.copy(scope[object]));
          for (var i=0; i < collection.length; i++){
            if (objectToDelete.id==collection[i].id)
              var index=i;
          }
          collection.splice(index, 1);
          storageSvc.postAll(collectionName,collection);
        })
        Directiva.reload($state,$location);
      })
    }
  }
})

//Borra objeto de una colección
Directiva.directive("mockupddDeleteAssociated", function($state,storageSvc) {
  return {
    scope: {
      prop: '@',
    },
    link: function(scope, element, attr) {
      var parts = attr.mockupddDeleteAssociated.split(" associatedWith ");
      var collectionOfAssociated = parts[0];
      objectAssociated = parts[1];
      $(element).on("click", function() {
        scope.$apply(function() {
          storageSvc.deleteAssociated(collectionOfAssociated,objectAssociated,scope.prop);
        })
      })
    }
  }
})

Directiva.directive("mockupddUpdateObject", function($state,storageSvc,$location) {
  return {
   scope:{
      data:'=',
    },
    link: function(scope, element, attr) {
      var parts = attr.mockupddUpdateObject.split(" is ");
      var objectUpdated = parts[0];
      var collectionName = parts[1];
      //var secondPart = parts[1].split(" withId ");
      //collectionName = secondPart[0];
      //operAndParam = secondPart[1];

      $(element).on("click", function() {
        scope.$apply(function() {
          var collection = [];
          collection = storageSvc.getAll(collectionName);
          //var id = Directiva.chooseOperBehaviour(operAndParam,$location);
          //updatedElem = scope.data;
          //updatedElem.id = id;
          //oldElem = storageSvc.getData(collectionName,id);
          for (var i=0; i < collection.length; i++){
            //if (oldElem[0].id==collection[i].id)
            if (scope.data.id==collection[i].id)
              var index=i;
          }
          collection.splice(index, 1);
          //collection.push(angular.copy(updatedElem));
          collection.push(angular.copy(scope.data));
          storageSvc.postAll(collectionName,collection);

        })

      })
    }
  }
})



//Muestra mensaje emergente con estilo. Se le envia como parámetro el mensaje a mostrar
Directiva.directive("mockupddMsg", function(SweetAlert) {
  return {
    link: function(scope, element, attr) {
      $(element).on("click",function(){
        SweetAlert.swal(attr.mockupddMsg);
      })
    }
  }
})

// Directiva que permite desplazarnos entre las distintas vistas independientemente si tienen parametros o no en la URL, puede utilizarse en  distintos tag html
Directiva.directive("mockupddNavigate", function($state,$location,$stateParams) {
  return {
    link: function(scope, element, attr) {
      var n = -1;
      var stateName = "";
      var parts = {};
      var paramss = {};
      var paramVale = "";
      var path = "";

      $(element).on("click", function() {
        n = attr.mockupddNavigate.search("/");
        if(n>=0){
          parts = attr.mockupddNavigate.split("/");
          stateName = parts[0];
          paramVale = parts[1];          
        }else{
          stateName = attr.mockupddNavigate;
        }

        scope.$apply(function() {
          if(paramVale  === ""){
            paramss = {};
            $location.path(stateName);
          }else{
            paramss[0]= paramVale;
            path =  stateName+"/"+paramss[0];
            $location.path(path);
          }
        })
        //$state.go(stateName, paramss, {reload: true} );
        Directiva.reload($state,$location);
      })      
    }
  }
})