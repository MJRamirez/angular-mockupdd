angular.module('cartControllers',[])
.controller('cartCtrl',function ($scope,$state,SweetAlert) {

	var desayuno = 30;
	var pedido = JSON.parse(localStorage['pedido']);
	$scope.checkboxBreakfast = pedido[0].breakfast;
	if ($scope.checkboxBreakfast) {
		$scope.precioTotal = parseInt(desayuno)+parseInt(pedido[2].price);
	} else {
		$scope.precioTotal = parseInt(pedido[2].price);
	}

	$scope.calculatePrice = function(){
		if ($scope.checkboxBreakfast) {
			$scope.precioTotal = parseInt(desayuno)+parseInt(pedido[2].price);
		} else {
			$scope.precioTotal = parseInt(pedido[2].price);
		}
		pedido[0].breakfast = $scope.checkboxBreakfast;
		localStorage['pedido'] = JSON.stringify(pedido);
	}

	$scope.pedido = pedido;

	$scope.editarResort = function () {
		$state.go('resorts');
	}

	$scope.editarRoom = function () {
		var pedido = JSON.parse(localStorage['pedido']);
		$state.go('rooms',{resortId : pedido[1].id});
	}

	$scope.checkout = function () {
		SweetAlert.swal("Compra realizada satisfactoriamente!","será redireccionado al principio", "success");
		$state.go('panel');
	}
});
