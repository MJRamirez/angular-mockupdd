angular.module('resortDetailControllers', [])
  .controller('resDetController', function($scope, _, $stateParams, $state) {

    $scope.guardarPedido = function(){
      var pedido = [];
      var pedidoBreakFast = JSON.parse(localStorage['pedido']);
      var resorts = JSON.parse(localStorage['resorts']);
      var resortTemp = _.where(resorts, {id: $stateParams.resortId});
      pedido.push(pedidoBreakFast[0]);
      pedido.push(resortTemp[0]);
      localStorage['pedido'] = JSON.stringify(pedido);
      $state.go('rooms', {resortId : $stateParams.resortId});
    }
});
