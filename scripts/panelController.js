angular.module('panelControllers',[])
.controller('SubmitForm', function ($scope,$state,SweetAlert,_){

  $scope.submit = function  () {
    if ((localStorage.getItem('resorts') == undefined) || (localStorage.getItem('rooms') == undefined)) {
      SweetAlert.swal("No resorts or rooms, please add some");
    }else{
      var pedidoTemp=[];
      if ($scope.checkboxBreakfast == undefined) {
        pedidoTemp.push({breakfast : false});
      } else {
        pedidoTemp.push({breakfast : $scope.checkboxBreakfast});
      }
      if ($scope.secondSelect != undefined){
        var rooms = JSON.parse(localStorage['rooms']);
        var resorts = JSON.parse(localStorage['resorts']);
        var roomPedido = _.where(rooms, {id: $scope.secondSelect});
        var resortPedido = _.where(resorts, {id: $scope.firstSelect});
        pedidoTemp.push(resortPedido[0]);
        pedidoTemp.push(roomPedido[0]);
        $state.go('cart');
      }else{
        if ($scope.firstSelect != undefined){
          var resorts = JSON.parse(localStorage['resorts']);
          var resortPedido = _.where(resorts, {id: $scope.firstSelect});
          pedidoTemp.push(resortPedido[0]);
          $state.go('rooms', {resortId : $scope.firstSelect});
        }else{
          $state.go('resorts')
        }
      }
      localStorage['pedido'] = JSON.stringify(pedidoTemp);
    }
  }
})

/*============================================
            Filtro dropdowns
=============================================*/
.filter('secondDropdown', function () {
    return function (secondSelect, firstSelect) {
        var filtered = [];
        if (firstSelect === null) {
            return filtered;
        }
        angular.forEach(secondSelect, function (s2) {
            if (s2.resortId == firstSelect) {
                filtered.push(s2);
            }
        });
        return filtered;
    };
})


//--------------------------------------Datepicker Controller---------------------------------//

.controller('panelCtrl', function ($scope) {

$scope.today = function() {
  $scope.dt = new Date();
};
$scope.today();

$scope.clear = function() {
  $scope.dt = null;
};

$scope.toggleMin = function() {
  $scope.minDate = $scope.minDate ? null : new Date();
};

$scope.toggleMin();
$scope.maxDate = new Date(2020, 5, 22);

$scope.open1 = function() {
  $scope.popup1.opened = true;
};

$scope.open2 = function() {
  $scope.popup2.opened = true;
};

$scope.setDate = function(year, month, day) {
  $scope.dt = new Date(year, month, day);
};

$scope.dateOptions = {
  formatYear: 'yy',
  startingDay: 1
};

$scope.popup1 = {
  opened: false
};

$scope.popup2 = {
  opened: false
};

$scope.getDayClass = function(date, mode) {
  if (mode === 'day') {
    var dayToCheck = new Date(date).setHours(0,0,0,0);

    for (var i = 0; i < $scope.events.length; i++) {
      var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

      if (dayToCheck === currentDay) {
        return $scope.events[i].status;
      }
    }
  }
  return '';
};
});
