var Directiva = angular.module('directivas', [])

Directiva.directive("mockupddGetData", function(storageSvc) {
  return {
      link: function(scope, element, attr) {
        var parts = attr.mockupddGetData.split(": ");
        var output = parts[0];
        var collectionName = parts[1];
      scope.output = storageSvc.getAll(collectionName);
    }
  }
})

//Guarda objeto
Directiva.directive("mockupddSave", function($state,storageSvc) {
  return {
    link: function(scope, element, attr) {
      var parts = attr.mockupddSave.split(" to ");
      var newObjectName = parts[0];
      var collectionName = parts[1];
      if (!scope[newObjectName]) {
        scope[newObjectName] = {};
      }
      $(element).on("click", function() {
        scope.$apply(function() {
        var collection = [];
        collection = storageSvc.getAll(collectionName);
        collection.push(angular.copy(scope[newObjectName]));
        storageSvc.postAll(collectionName,collection);
        scope[newObjectName] = {};
        $state.reload();
        })
      })
    }
  }
})

Directiva.directive("mockupddDelete", function($state,storageSvc) {
  return {
    link: function(scope, element, attr) {
      var parts = attr.mockupddDelete.split(" in ");
      var object = parts[0];
      var collectionName = parts[1];
      $(element).on("click", function() {
        scope.$apply(function() {
          var collection = storageSvc.getAll(collectionName);
          var objectToDelete = (angular.copy(scope[object]));
          for (var i=0; i < collection.length; i++){
            if (objectToDelete.id==collection[i].id)
              var index=i;
          }
          collection.splice(index, 1);
          storageSvc.postAll(collectionName,collection);
        })
        $state.reload();
      })
    }
  }
})

Directiva.directive("mockupddMsg", function(SweetAlert) {
  return {
    link: function(scope, element, attr) {
      $(element).on("click",function(){
        SweetAlert.swal(attr.mockupddMsg);
      })
    }
  }
})
